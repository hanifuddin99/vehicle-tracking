<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<title></title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
     integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
     crossorigin=""/>
<style>
    #map { height: 100vh; }
</style>
</head>
<body>
<div id="map"></div>


<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
     integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
     crossorigin=""></script>
<script>
    var map = L.map('map');
    map.setView([51.505, -0.09], 13);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

navigator.geolocation.watchPosition(success, error);
function success(pos){
    const lat = pos.coords.latitude;
    const longi = pos.coords.longitude;
    const accuracy = pos.coords.accuracy;

    let marker = L.marker([lat, longi]).addTo(map);
    let circle = L.circle([lat, longi], { radius: accuracy }).addTo(map);

    map.fitBounds(circle.getBounds());
}
function error(err){
    if(err.code === 1){
        alert("Please allow geolocation access");
    }else{
        alert("Cann't get current location");
    }
}
</script>     
       
</body>
</html>
